package org.craft11.greenthumb;

import org.bukkit.block.Block;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerListener;

import org.craft11.API;
import org.nfcmmo.Users;
import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.skills.Skills;

import java.util.Random;

public class Listener extends PlayerListener {
    @Override
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block b = event.getClickedBlock();

            if (API.IsHoe(event.getPlayer().getItemInHand()) && b.getTypeId() == 59 && b.getData() == 7) {
                b.setData((byte) 0);

                Random rnd = new Random();
                int seedAmt = rnd.nextInt(3);

                if (seedAmt > 0)
                    API.DropItem(b.getLocation().add(0.0,1.0,0.0), 295, seedAmt);

                API.DropItem(b.getLocation().add(0.0, 1.0, 0.0), 296, 1);

                if (GreenThumb.nfcMMOEnabled) {
                    Users.getProfile(event.getPlayer()).addXP(SkillType.HERBALISM, Config.mwheat);
                    Skills.XpCheckSkill(SkillType.HERBALISM, event.getPlayer());
                }
                API.DamageTool(event.getPlayer(), 1);
            }
        }
    }
}
