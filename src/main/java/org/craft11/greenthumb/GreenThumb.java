package org.craft11.greenthumb;

import org.bukkit.event.Event;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class GreenThumb extends JavaPlugin {
    Listener thisListener = new Listener();

    public static boolean nfcMMOEnabled;

    @Override
    public void onDisable() {
        this.getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvent(Event.Type.PLAYER_INTERACT, thisListener, Event.Priority.Monitor, this);
        if (pm.getPlugin("nfcMMO") != null) {
            nfcMMOEnabled = true;
        }
    }
}
